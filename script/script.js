/* Теоретичні питання
1. Опишіть своїми словами як працює метод forEach.
Метод forEach з допомогою callBack функції перебирає всі елементи масиву без повернення нового масиву.

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.
Методами які мутують массив являються (pop, push, shift, unshift, splice).
Методи які не мутують массив (slice, concat, map, filter, reduce, sort, reverse).

3. Як можна перевірити, що та чи інша змінна є масивом?
Метод Array.isArray() дозволяє визначити, чи є конкретне значення масивом, а також за допомогою оператора typeof.

4. В яких випадках краще використовувати метод map(), а в яких forEach()?
Метод forEach() впливає на оригінальний масив і змінює його, а метод map() повертає зовсім новий масив, залишаючи вихідний масив незмінним.

Практичні завдання
1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.*/

let strArray = ["travel", "hello", "eat", "ski", "lift"];
let strTotal = strArray.filter(str => str.length > 3);
console.log(strTotal);

/*2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. 
Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча". Відфільтрований масив виведіть в консоль.*/

const humans = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Василь", age: 35, sex: "чоловіча"},
    {name: "Іванка", age: 27, sex: "жіноча"},
    {name: "Василина", age: 15, sex: "жіноча"}
];
let humanMale = humans.filter(human => human.sex === "чоловіча");
console.log(humanMale);

/*3. Реалізувати функцію фільтру масиву за вказаним типом даних.*/

function filterBy(arr, dataType) {
    return arr.filter(arrItem => typeof arrItem !== dataType);
};
console.log(filterBy(['hello', 'world', 23, '23', null], "string"));